#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

function validate_and_move () {
  local file="$1"

  echo "Validating downloaded file: ${SCRIPT_DIR}/repos/$file.tmp"
  # Validate file before override existing
  if gpg --show-keys "${SCRIPT_DIR}/repos/$file.tmp" 2>&1 > /dev/null ; then
    mv "${SCRIPT_DIR}/repos/$file.tmp" "${SCRIPT_DIR}/repos/$file"
  else
    echo "File downloaded does not seem like a valid gpg key : ${SCRIPT_DIR}/repos/$file.tmp"
  fi

  echo "GPG key details of the current file:"
  gpg --show-keys "${SCRIPT_DIR}/repos/$file"
}

function update_key_file_from_url () {
  local file="$1"
  local url="$2"

  echo
  echo "update_key_file_from_url File: repos/$file"

  curl -s -S -L "$url" -o "${SCRIPT_DIR}/repos/$file.tmp"

  # De-armor teh file
  if grep -q "KEY BLOCK" "${SCRIPT_DIR}/repos/$file.tmp" ; then
    cat "${SCRIPT_DIR}/repos/$file.tmp" | gpg --dearmor -o "${SCRIPT_DIR}/repos/$file.tmp2"
    mv "${SCRIPT_DIR}/repos/$file.tmp2" "${SCRIPT_DIR}/repos/$file.tmp"
  fi

  validate_and_move "$file"
}

function update_key_file_from_keyserver () {
  local file="$1"
  local key="$2"
  local keyserver="$3"

  local tmpkeyring="${SCRIPT_DIR}/repos/tmpkeyring"

  echo
  echo "update_key_file_from_keyserver File: repos/$file"

  if [ -f "${tmpkeyring}" ] ; then
    rm "${tmpkeyring}"
  fi

  gpg --quiet --batch --no-default-keyring --keyring "${tmpkeyring}" --keyserver "${keyserver}" --recv-key "${key}"
  gpg --quiet --batch --no-default-keyring --keyring "${tmpkeyring}" --output "${SCRIPT_DIR}/repos/$file.tmp" --export

  validate_and_move "$file"
}

# This is the only key with expiration, currently set to 2026-02-04
update_key_file_from_url wikisuite-php_debian.gpg https://packages.sury.org/php/apt.gpg

# There is no expire date, so only if the key is canceled
update_key_file_from_url wikisuite-elasticsearch.gpg https://artifacts.elastic.co/GPG-KEY-elasticsearch
update_key_file_from_url wikisuite-manticore.gpg https://repo.manticoresearch.com/GPG-KEY-manticore
update_key_file_from_url wikisuite-nodejs.gpg https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key
update_key_file_from_url wikisuite-syncthing.gpg https://syncthing.net/release-key.gpg
update_key_file_from_keyserver wikisuite-php_ubuntu.gpg 14AA40EC0831756756D7F66C4F4EA0AAE5267A6C keyserver.ubuntu.com
